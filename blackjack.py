#Blackjack

import pygame
import time
import random

clock = pygame.time.Clock()

cards = ['a',2,3,4,5,6,7,8,9,10,'j','q','k']

suitA = ''
suitB = ''
suitC = ''
suitD = ''
suitE = ''

dsuitA = ''
dsuitB = ''
dsuitC = ''

cardA = ''
cardB = ''
cardC = ''
cardD = ''
cardE = ''

dcardA = ''
dcardB = ''
dcardC = ''

hit = 0
bal = 100

dealerHand = [dsuitA, dcardA, dsuitB, dsuitB, dsuitC, dcardC]

randomNumber = 0
randomNumber2 = 0
randomNumber3 = 0

c1v = 0
c2v = 0
c3v = 0
c4v = 0
c5v = 0

d1v = 0
d2v = 0
d3v = 0
d4v = 0
d5v = 0

bust = False
blackjack = False

white = (225,225,225)
black = (0,0,0)
red = (225,0,0)
green = (0,220,0)
grey = (128,128,128)
purple = (128,0,128)

display_width = 960
display_height = 640

win = False
dealt = False

deckx = ((display_width/2)-20)
decky = ((2*display_height)/3)

def text_objects(text, font):
    textSurface = font.render(text, True, white)
    return textSurface, textSurface.get_rect()

def text_objects2(text, font):
    textSurface2 = font.render(text, True, black)
    return textSurface2, textSurface2.get_rect()

def titleText(text):
    largeText = pygame.font.Font('freesansbold.ttf', 55)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((display_width/2),(display_height/3))
    gameDisplay.blit(TextSurf, TextRect)

def c1(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects2(text, largeText)
    TextRect.center = ((600),(595))
    gameDisplay.blit(TextSurf, TextRect)

def c2(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects2(text, largeText)
    TextRect.center = ((540),(595))
    gameDisplay.blit(TextSurf, TextRect)

def c3(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects2(text, largeText)
    TextRect.center = ((480),(595))
    gameDisplay.blit(TextSurf, TextRect)

def c4(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects2(text, largeText)
    TextRect.center = ((420),(595))
    gameDisplay.blit(TextSurf, TextRect)

def c5(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects2(text, largeText)
    TextRect.center = ((360),(595))
    gameDisplay.blit(TextSurf, TextRect)

def balance(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((display_width/2+200),(display_height/16))
    gameDisplay.blit(TextSurf, TextRect)

def value(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects2(text, largeText)
    TextRect.center = ((480),(530))
    gameDisplay.blit(TextSurf, TextRect)

def playButton(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((display_width/4),(2*display_height/3))
    gameDisplay.blit(TextSurf, TextRect)

def orDontButton(text):
    largeText = pygame.font.Font('freesansbold.ttf', 25)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((3*display_width/4),(2*display_height/3))
    gameDisplay.blit(TextSurf, TextRect)

gameDisplay = pygame.display.set_mode((display_width, display_height))

on = True
pregame = True
play = False
onePlayer = False
twoPlayer = False

while on == True:
    pygame.init()

    while pregame == True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pregame = False
                on = False

        gameDisplay.blit(pygame.image.load(r'titleScreen.png'),(0,0))

        mouse = pygame.mouse.get_pos()

        pygame.mouse.set_visible(0)

        titleText('Blackjack but Awesome')

        playButton('Play')
        orDontButton('Or Dont')

        gameDisplay.blit(pygame.image.load(r'crosshair.png'),(mouse[0]-15,mouse[1]-15,29,29))

        if pygame.mouse.get_pressed()[0] and 210 < mouse[0] < 270 and 415 < mouse[1] < 440:
            play = True
            pregame = False
            on = False

        if pygame.mouse.get_pressed()[0] and 675 < mouse[0] < 770 and 415 < mouse[1] < 440:
            pregame = False
            on = False

        pygame.display.update()

        clock.tick(144)

    while play == True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                play = False
                on = False

        mouse = pygame.mouse.get_pos()
        #minimize this for ease of navigation
        if win == False:

            if pygame.mouse.get_pressed()[0] and (display_width/2)-20 < mouse[0] < (display_width/2)+20 and 440 < mouse[1] < 465:

                randomNumber = random.random() * 100
                randomNumber2 = random.random() * 100
                randomNumber3 = random.random() * 100

                if dealt == False:
                    if randomNumber < 25:
                        suitA = 'h'
                        suitB = 'd'

                    elif 25 < randomNumber < 50:
                        suitA = 'd'
                        suitB = 's'

                    elif 50 < randomNumber < 75:
                        suitA = 'c'
                        suitB = 'h'

                    else:
                        suitA = 's'
                        suitB = 'c'

                    if randomNumber2 < 7.69:
                        cardA = cards[0]

                    elif 7.69 < randomNumber2 < 15.38:
                        cardA = str(cards[1])

                    elif 15.38 < randomNumber2 < 23.07:
                        cardA = str(cards[2])

                    elif 23.07 < randomNumber2 < 30.76:
                        cardA = str(cards[3])

                    elif 30.76 < randomNumber2 < 38.45:
                        cardA = str(cards[4])

                    elif 38.45 < randomNumber2 < 46.14:
                        cardA = str(cards[5])

                    elif 46.14 < randomNumber2 < 53.83:
                        cardA = str(cards[6])

                    elif 53.83 < randomNumber2 < 61.52:
                        cardA = str(cards[7])

                    elif 61.52 < randomNumber2 < 69.21:
                        cardA = str(cards[8])

                    elif 69.21 < randomNumber2 < 76.9:
                        cardA = str(cards[9])

                    elif 76.9 < randomNumber2 < 84.59:
                        cardA = str(cards[10])

                    elif 84.59 < randomNumber2 < 92.28:
                        cardA = str(cards[11])

                    else:
                        cardA = str(cards[12])

                    if randomNumber3 < 7.69:
                        cardB = cards[0]

                    elif 7.69 < randomNumber3 < 15.38:
                        cardB = str(cards[1])

                    elif 15.38 < randomNumber3 < 23.07:
                        cardB = str(cards[2])

                    elif 23.07 < randomNumber3 < 30.76:
                        cardB = str(cards[3])


                    elif 30.76 < randomNumber3 < 38.45:
                        cardB = str(cards[4])

                    elif 38.45 < randomNumber3 < 46.14:
                        cardB = str(cards[5])

                    elif 46.14 < randomNumber3 < 53.83:
                        cardB = str(cards[6])

                    elif 53.83 < randomNumber3 < 61.52:
                        cardB = str(cards[7])

                    elif 61.52 < randomNumber3 < 69.21:
                        cardB = str(cards[8])

                    elif 69.21 < randomNumber3 < 76.9:
                        cardB = str(cards[9])

                    elif 76.9 < randomNumber3 < 84.59:
                        cardB = str(cards[10])

                    elif 84.59 < randomNumber3 < 92.28:
                        cardB = str(cards[11])

                    else:
                        cardB = str(cards[12])

                randomNumber = random.random() * 100
                randomNumber2 = random.random() * 100
                randomNumber3 = random.random() * 100

                if dealt == False:

                    if randomNumber < 25:
                        dsuitA = 'h'
                        dsuitB = 'd'

                    elif 25 < randomNumber < 50:
                        dsuitA = 'd'
                        dsuitB = 's'

                    elif 50 < randomNumber < 75:
                        dsuitA = 'c'
                        dsuitB = 'h'

                    else:
                        dsuitA = 's'
                        dsuitB = 'c'

                    if randomNumber2 < 7.69:
                        dcardA = cards[0]

                    elif 7.69 < randomNumber2 < 15.38:
                        dcardA = str(cards[1])

                    elif 15.38 < randomNumber2 < 23.07:
                        dcardA = str(cards[2])

                    elif 23.07 < randomNumber2 < 30.76:
                        dcardA = str(cards[3])

                    elif 30.76 < randomNumber2 < 38.45:
                        dcardA = str(cards[4])

                    elif 38.45 < randomNumber2 < 46.14:
                        dcardA = str(cards[5])

                    elif 46.14 < randomNumber2 < 53.83:
                        dcardA = str(cards[6])

                    elif 53.83 < randomNumber2 < 61.52:
                        dcardA = str(cards[7])

                    elif 61.52 < randomNumber2 < 69.21:
                        dcardA = str(cards[8])

                    elif 69.21 < randomNumber2 < 76.9:
                        dcardA = str(cards[9])

                    elif 76.9 < randomNumber2 < 84.59:
                        dcardA = str(cards[10])

                    elif 84.59 < randomNumber2 < 92.28:
                        dcardA = str(cards[11])

                    else:
                        dcardA = str(cards[12])

                    if randomNumber3 < 7.69:
                        dcardB = cards[0]

                    elif 7.69 < randomNumber3 < 15.38:
                        dcardB = str(cards[1])

                    elif 15.38 < randomNumber3 < 23.07:
                        dcardB = str(cards[2])

                    elif 23.07 < randomNumber3 < 30.76:
                        dcardB = str(cards[3])

                    elif 30.76 < randomNumber3 < 38.45:
                        dcardB = str(cards[4])

                    elif 38.45 < randomNumber3 < 46.14:
                        dcardB = str(cards[5])

                    elif 46.14 < randomNumber3 < 53.83:
                        dcardB = str(cards[6])

                    elif 53.83 < randomNumber3 < 61.52:
                        dcardB = str(cards[7])

                    elif 61.52 < randomNumber3 < 69.21:
                        dcardB = str(cards[8])

                    elif 69.21 < randomNumber3 < 76.9:
                        dcardB = str(cards[9])

                    elif 76.9 < randomNumber3 < 84.59:
                        dcardB = str(cards[10])

                    elif 84.59 < randomNumber3 < 92.28:
                        dcardB = str(cards[11])

                    else:
                        dcardB = str(cards[12])

                hand = [suitA, cardA, suitB, cardB, suitC, cardC, suitD, cardD]
                dealerHand = [dsuitA, dcardA, dsuitB, dcardB]
                print(str(hand))
                print(str(dealerHand))
                dealt = True

        gameDisplay.blit(pygame.image.load(r'1playerBackground.png'),(0,0))
        gameDisplay.blit(pygame.image.load(r'deck.png'),(deckx,decky))

        if dealt == True:

            if hand[0] == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(580,560))

            elif hand[0] == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(580,560))

            elif hand[0] == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(580,560))

            elif hand[0] == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(580,560))

            if hand[2] == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(520,560))

            elif hand[2] == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(520,560))

            elif hand[2] == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(520,560))

            elif hand[2] == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(520,560))

            if hand[1] == 'a':
                c1v = 11

            elif hand[1] == 'j':
                c1v = 10

            elif hand[1] == 'q':
                c1v = 10

            elif hand[1] == 'k':
                c1v = 10

            else:
                c1v = int(hand[1])

            if hand[3] == 'a':
                c2v = 11

            elif hand[3] == 'j':
                c2v = 10

            elif hand[3] == 'q':
                c2v = 10

            elif hand[3] == 'k':
                c2v = 10

            else:
                c2v = int(hand[3])

            c1(cardA)
            c2(cardB)
            valueCalculator = c1v + c2v + c3v + c4v + c5v

            value(str(valueCalculator))

            if hit == 0:
                gameDisplay.blit(pygame.image.load(r'Hit.png'),(470,570))

            elif hit == 1:
                gameDisplay.blit(pygame.image.load(r'Hit.png'),(410,570))

            elif hit == 2:
                gameDisplay.blit(pygame.image.load(r'Hit.png'),(350,570))

            if pygame.mouse.get_pressed()[0] and 460 < mouse[0] < 500 and 550 < mouse[1] < 610 and hit == 0 and dealt == True:
                randomNumber = random.random() * 100
                randdomNumber2 = random.random() * 100

                if randomNumber < 25:
                    suitC = 'h'

                elif 25 < randomNumber < 50:
                    suitC = 'd'

                elif 50 < randomNumber < 75:
                    suitC = 'c'

                else:
                    suitC = 's'

                if randomNumber2 < 7.69:
                    cardC = cards[0]

                elif 7.69 < randomNumber2 < 15.38:
                    cardC = str(cards[1])

                elif 15.38 < randomNumber2 < 23.07:
                    cardC = str(cards[2])

                elif 23.07 < randomNumber2 < 30.76:
                    cardC = str(cards[3])

                elif 30.76 < randomNumber2 < 38.45:
                    cardC = str(cards[4])

                elif 38.45 < randomNumber2 < 46.14:
                    cardC = str(cards[5])

                elif 46.14 < randomNumber2 < 53.83:
                    cardC = str(cards[6])

                elif 53.83 < randomNumber2 < 61.52:
                    cardC = str(cards[7])

                elif 61.52 < randomNumber2 < 69.21:
                    cardC = str(cards[8])

                elif 69.21 < randomNumber2 < 76.9:
                    cardC = str(cards[9])

                elif 76.9 < randomNumber2 < 84.59:
                    cardC = str(cards[10])

                elif 84.59 < randomNumber2 < 92.28:
                    cardC = str(cards[11])

                else:
                    cardC = str(cards[12])

                if cardC == 'a':
                    c3v = 11

                elif cardC == 'j':
                    c3v = 10

                elif cardC == 'q':
                    c3v = 10

                elif cardC == 'k':
                    c3v = 10

                else:
                    c3v = int(cardC)

                hit += 1

            randomNumber = random.random() * 100
            randomNumber2 = random.random() * 100

            print(mouse)

            if pygame.mouse.get_pressed()[0] and 400 < mouse[0] < 440 and 560 < mouse[1] < 620 and hit == 1 and dealt == True:

                if randomNumber < 25:
                    suitD = 'h'

                elif 25 < randomNumber < 50:
                    suitD = 'd'

                elif 50 < randomNumber < 75:
                    suitD = 'c'

                else:
                    suitD = 's'

                if randomNumber2 < 7.69:
                    cardD = cards[0]

                elif 7.69 < randomNumber2 < 15.38:
                    cardD = str(cards[1])

                elif 15.38 < randomNumber2 < 23.07:
                    cardD = str(cards[2])

                elif 23.07 < randomNumber2 < 30.76:
                    cardD = str(cards[3])

                elif 30.76 < randomNumber2 < 38.45:
                    cardD = str(cards[4])

                elif 38.45 < randomNumber2 < 46.14:
                    cardD = str(cards[5])

                elif 46.14 < randomNumber2 < 53.83:
                    cardD = str(cards[6])

                elif 53.83 < randomNumber2 < 61.52:
                    cardD = str(cards[7])

                elif 61.52 < randomNumber2 < 69.21:
                    cardD = str(cards[8])

                elif 69.21 < randomNumber2 < 76.9:
                    cardD = str(cards[9])

                elif 76.9 < randomNumber2 < 84.59:
                    cardD = str(cards[10])

                elif 84.59 < randomNumber2 < 92.28:
                    cardD = str(cards[11])

                else:
                    cardD = str(cards[12])

                if cardD == 'a':
                    c4v = 11

                elif cardD == 'j':
                    c4v = 10

                elif cardD == 'q':
                    c4v = 10

                elif cardD == 'k':
                    c4v = 10

                else:
                    c4v = int(cardD)

                pygame.time.wait(100)

                hit += 1

        if blackjack == True:
            dealt = False

        if hit >= 1:
            if suitC == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(460,560))

            if suitC == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(460,560))

            if suitC == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(460,560))

            if suitC == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(460,560))

        if hit >= 2:
            if suitD == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(400,560))

            if suitD == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(400,560))

            if suitD == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(400,560))

            if suitD == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(400,560))

        if pygame.mouse.get_pressed()[0] and 340 < mouse[0] < 380 and 560 < mouse[1] < 620 and hit == 2 and dealt == True:

            if randomNumber < 25:
                suitE = 'h'

            elif 25 < randomNumber < 50:
                suitE = 'd'

            elif 50 < randomNumber < 75:
                suitE = 'c'

            else:
                suitE = 's'

            if randomNumber2 < 7.69:
                cardE = cards[0]

            elif 7.69 < randomNumber2 < 15.38:
                cardE = str(cards[1])

            elif 15.38 < randomNumber2 < 23.07:
                cardE = str(cards[2])

            elif 23.07 < randomNumber2 < 30.76:
                cardE = str(cards[3])

            elif 30.76 < randomNumber2 < 38.45:
                cardE = str(cards[4])

            elif 38.45 < randomNumber2 < 46.14:
                cardE = str(cards[5])

            elif 46.14 < randomNumber2 < 53.83:
                cardE = str(cards[6])

            elif 53.83 < randomNumber2 < 61.52:
                cardE = str(cards[7])

            elif 61.52 < randomNumber2 < 69.21:
                cardE = str(cards[8])

            elif 69.21 < randomNumber2 < 76.9:
                cardE = str(cards[9])

            elif 76.9 < randomNumber2 < 84.59:
                cardE = str(cards[10])

            elif 84.59 < randomNumber2 < 92.28:
                cardE = str(cards[11])

            else:
                cardE = str(cards[12])

            if cardE == 'a':
                c5v = 11

            elif cardE == 'j':
                c5v = 10

            elif cardE == 'q':
                c5v = 10

            elif cardE == 'k':
                c5v = 10

            else:
                c5v = int(cardE)

            hit += 1

        if blackjack == True:
            dealt = False

        if hit >= 1:
            if suitC == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(460,560))

            if suitC == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(460,560))

            if suitC == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(460,560))

            if suitC == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(460,560))

        if hit >= 2:
            if suitD == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(400,560))

            if suitD == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(400,560))

            if suitD == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(400,560))

            if suitD == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(400,560))

        if hit >= 3:
            if suitE == 'h':
                gameDisplay.blit(pygame.image.load(r'hearts.png'),(400,560))

            if suitE == 'd':
                gameDisplay.blit(pygame.image.load(r'diamonds.png'),(400,560))

            if suitE == 'c':
                gameDisplay.blit(pygame.image.load(r'clubs.png'),(400,560))

            if suitE == 's':
                gameDisplay.blit(pygame.image.load(r'spades.png'),(400,560))


        valueCalculator = c1v + c2v + c3v + c4v + c5v

        if valueCalculator > 21:
            if cardA == 'a':
                c1v = 1
            if cardB == 'a':
                c2v = 1
            if cardC == 'a':
                c3v = 1
            if cardD == 'a':
                c4v = 1

            valueCalculator = c1v + c2v + c3v + c4v + c5v

            if valueCalculator > 21:
                dealt = False
                c1v = 0
                c2v = 0
                c3v = 0
                c4v = 0
                c5v = 0
                hit = 0
                bal -= 50

        if valueCalculator == 21:
            dealt = False
            c1v = 0
            c2v = 0
            c3v = 0
            c4v = 0
            c5v = 0
            hit = 0
            bal += 50

        else:
            if hit >= 1:
                c3(cardC)
            if hit >= 2 :
                c4(cardD)
            if hit >= 3:
                c5(cardE)

        balance('Balance: ' + str(bal))

        if pygame.mouse.get_pressed()[0] and 200 < mouse[0] < 270 and 550 < mouse[1] < 570 and dealt == True:
            if valueCalculator == 14:
                randomNumber = random.random() * 100
                if randomNumber < 10:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0
                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            if valueCalculator == 15:
                randomNumber = random.random() * 100
                if randomNumber < 20:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0
                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            if valueCalculator == 16:
                randomNumber = random.random() * 100
                if randomNumber < 27:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            if valueCalculator == 17:
                randomNumber = random.random() * 100
                if randomNumber < 35:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            if valueCalculator == 18:
                randomNumber = random.random() * 100
                if randomNumber < 45:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0
                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            if valueCalculator == 19:
                randomNumber = random.random() * 100
                if randomNumber < 70:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            if valueCalculator == 20:
                randomNumber = random.random() * 100
                if randomNumber < 85:
                    bal += 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

                else:
                    bal -= 50
                    dealt = False
                    c1v = 0
                    c2v = 0
                    c3v = 0
                    c4v = 0
                    c5v = 0
                    hit = 0

            elif valueCalculator <= 13:
                bal -= 50
                dealt = False
                c1v = 0
                c2v = 0
                c3v = 0
                c4v = 0
                c5v = 0
                hit = 0

        gameDisplay.blit(pygame.image.load(r'stand.png'),(200, 550))
        gameDisplay.blit(pygame.image.load(r'crosshair.png'),(mouse[0]-15,mouse[1]-15))

        pygame.display.update()
